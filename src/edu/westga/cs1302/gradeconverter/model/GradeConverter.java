package edu.westga.cs1302.gradeconverter.model;

/**
 * Class that converts numerical grades to letter grades.
 * 
 * @author CS1302
 * 
 */
public class GradeConverter {

	public static final String GRADE_OUT_OF_RANGE = "grade out of range - range 0 to 100, inclusive.";

	/**
	 * Convert the specified numeric grade to the appropriate letter grade. 
	 * A: 90-100 B: 80-89 C: 70-79 D: 60-69 F: 0-59
	 * 
	 * @precondition 0 <= grade <= 100
	 * @postcondition none
	 * 
	 * @param grade
	 *            numeric grade to be converted
	 * 
	 * @return The appropriate letter grade
	 */
	public String convertToLetterGrade(int grade) {
		if (grade < 0 || grade > 100) {
			throw new IllegalArgumentException(GRADE_OUT_OF_RANGE);
		}
		
		String letter = "";

		if (grade >= 90) {
			letter = "A";
		} else if (grade >= 80) {
			letter = "B";
		} else if (grade >= 70) {
			letter = "C";
		} else if (grade >= 60) {
			letter = "D";
		} else {
			letter = "F";
		}

		return letter;
	}

	/**
	 * Determine and return the appropriate plus or minus (or neither) dependent on
	 * the grade provided.
	 * 
	 * +: 97-100, 87-89, 77-79, 67-69, 57-59 -: 90-92, 80-82, 70-72, 60-62, 0-52
	 * 
	 * All other options return an empty string
	 * 
	 * @precondition 0 <= grade <= 100
	 * @postcondition none
	 * 
	 * @param grade
	 *            numeric grade
	 * 
	 * @return "+" || "-" || ""
	 */
	public String getPlusOrMinus(int grade) {
		if (grade < 0 || grade > 100) {
			throw new IllegalArgumentException(GRADE_OUT_OF_RANGE);
		}
		
		int gradeRange = grade % 10;

		if (gradeRange >= 7) {
			return "+";
		} else if (gradeRange <= 2) {
			return "-";
		} else {
			return "";
		}
	}
}
