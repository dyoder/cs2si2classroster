package edu.westga.cs1302.gradeconverter.model;

import java.util.ArrayList;

/**
 * Defines a Roster.
 *
 * @author CS1302
 */
public class Roster {

	private ArrayList<Student> students;

	/**
	 * Makes a class roster.
	 *
	 * @precondition none
	 * @postcondition size() == size()@prev + 1
	 */
	public Roster() {
		this.students = new ArrayList<Student>();
	}

	/**
	 * Size of class roster.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return The size of the class roster
	 */
	public int size() {
		return this.students.size();
	}

	/**
	 * Adds the given student to the roster.
	 * 
	 * @precondition student != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param student
	 *            The student object to add to the roster
	 * 
	 * @return true if student successfully added, false otherwise
	 */
	public boolean add(Student student) {
		// TODO Complete this method as part of SI2
		return false;
	}

	/**
	 * Counts all the students who have a grade between the specified lower and
	 * upper grades, inclusive.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param lowGrade
	 *            the low grade
	 * @param highGrade
	 *            the high grade
	 * 
	 * @return Count of number of grades between lowGrade and highGrade inclusive.
	 * 
	 */
	public int countGradesBetween(int lowGrade, int highGrade) {
		// TODO Complete as part of SI2 exercise
		return 0;
	}

}
